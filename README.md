# react-firebase-team1

Dibuat oleh KELOMPOK 1 Binar FSW wave 16
Anggota kelompok terdiri dari:

1. Rinaldi
2. Athalia Beatrice Boedianto
3. Fernandre Kurniawan
4. Cipto Widarto
5. Zilian Rifaldo
6. Riski Dwi Nugroho

## Role

1. Koordinator / Scrum Master : Rinaldi
2. Repo Maintainer front-end : Cipto Widarto
3. Repo Maintainer back-end : Fernandre Kurniawan
4. Boilerplate Coder front-end : Riski Dwi Nugroho
5. Boilerplate Coder back-end : Athalia Beatrice Boedianto
6. Devops : Zilian Rifaldo

## Deskripsi Kerja

1. Rinaldi

   - membuat grup telegram & akun trello untuk team koordinasi
   - desain Profile page
   - membuat tampilan frontend Profile page

2. Athalia Beatrice Boedianto

   - desain Landing page
   - membuat tampilan frontend Landing page
   - membuat footer

3. Fernandre Kurniawan

   - desain Home page
   - membuat tampilan frontend Home page
   - membuat tampilan game Rock Paper Scissors
   - membuat navbar & footer
   - integrasi Home page dengan firebase
   - integrasi Profile page dengan firebase
   - integrasi Rock Paper Scissors page dengan firebase
   - integrasi Game detail page dengan firebase

4. Cipto Widarto

   - desain Register & Login page
   - membuat tampilan frontend Register & Login page
   - setup firebase di react
   - running fungsi register email & password, simpan data user di firestore
   - running auth login email & password, middleware Home page
   - integrasi Landing page dengan firebase
   - integrasi Game list page dengan firebase
   - integrasi Game detail page dengan firebase
   - handling page not found
   - running sign in with Google

5. Zilian Rifaldo

   - desain Game detail page
   - membuat tampilan frontend Game detail page
   - membuat navbar

6. Riski Dwi Nugroho
   - membuat Boilerplate react
   - desain Game list page
   - membuat tampilan frontend Game list page
   - membuat footer
