import Home from "./pages/Home";
import Updates from "./pages/Updates";
import Profile from "./pages/Profile";
import RPS from "./pages/RPS";
import Landingpage from "./pages/Landingpage";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Gamedetail from "./pages/Gamedetail";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NotFound from "./pages/NotFound";
import List from "./pages/List";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/updates/:id" element={<Updates />} />
        <Route path="/profile/:id" element={<Profile />} />
        <Route path="/rps" element={<RPS />} />
        <Route path="/" element={<Landingpage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/detailgame/:id" exact element={<Gamedetail />} />
        <Route path="/game-list" exact element={<List />} />
      </Routes>
    </Router>
  );
}

export default App;
