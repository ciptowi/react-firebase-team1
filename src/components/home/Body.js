import React from "react";
import { Container, Row, Col } from "reactstrap";
import LastPlayedGame from "./LastPlayedGame";
import Updates from "./Updates";
import Leaderboards from "./Leaderboards";
import Statistics from "./Statistics";

const HomeBody = () => {
  return (
    <Container>
      <Row>
        <Col sm={8}>
          <LastPlayedGame></LastPlayedGame>
        </Col>
        <Col sm={4}>
          <Leaderboards></Leaderboards>
        </Col>
      </Row>
      <Row>
        <Col sm={8}>
          <Updates></Updates>
        </Col>
        <Col sm={4}>
          <Statistics></Statistics>
        </Col>
      </Row>
    </Container>
  );
};
export default HomeBody;
