import React from "react";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import { auth, db } from "../../config/firebase";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { setDoc, doc } from "firebase/firestore";

function ButtonRegister(props) {
  const username = props.username;
  const email = props.email;
  const password = props.password;
  const setAlert = props.setAlert;
  const navigate = useNavigate();

  const register = async (e) => {
    e.preventDefault();
    if (username === "" || email === "" || password === "") {
      setAlert("Data tidak boleh kosong !!");
    }
    if (password.length < 6) {
      setAlert("Password minimal 6 karakter !!");
    } else {
      try {
        const res = await createUserWithEmailAndPassword(auth, email, password);
        const user = res.user;
        const docRef = doc(db, "users", user.uid);
        await setDoc(docRef, {
          username: username,
          email: email,
          profile_picture: "/images/user.png",
          level: "Easy",
          authProvider: "local",
          score: 0,
          last_play: new Date(),
        });
        localStorage.setItem("token", user.accessToken);
        alert("Registrasi anda berhasil, silahkan Login");
        navigate("/login");
      } catch (err) {
        alert(err.message);
      }
    }
  };
  return (
    <div className="text-center my-4">
      <Button onClick={register} color="primary">
        Register
      </Button>
    </div>
  );
}

export default ButtonRegister;
