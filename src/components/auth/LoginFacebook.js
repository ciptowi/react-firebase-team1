import React from "react";
import { Button } from "reactstrap";
import { signInWithPopup, FacebookAuthProvider } from "firebase/auth";
import { auth } from "../../config/firebase";

function LoginFacebook() {
  const facebookProvider = new FacebookAuthProvider();
  const loginFacebook = async () => {
    signInWithPopup(auth, facebookProvider)
      .then((result) => {
        // The signed-in user info.
        //const user = result.user;
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        const credential = FacebookAuthProvider.credentialFromResult(result);
        const accessToken = credential.accessToken;
        // ...
        localStorage.setItem("token", accessToken);
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.email;
        // The AuthCredential type that was used.
        const credential = FacebookAuthProvider.credentialFromError(error);
        alert(errorCode, errorMessage, email, credential);
      });
  };
  return (
    <div className="text-center">
      <Button className="px-4" outline color="primary" onClick={loginFacebook}>
        <img src="./images/fb.png" width="30px" className="mx-2" alt="facebook" />
        Continue with Facebook
      </Button>
    </div>
  );
}

export default LoginFacebook;
