import React from "react";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import { auth, db } from "../../config/firebase";
import { signInWithEmailAndPassword } from "firebase/auth";
import { getDoc, doc } from "firebase/firestore";

function ButtonLogin(props) {
  const email = props.email;
  const password = props.password;
  const setAlert = props.setAlert;
  const navigate = useNavigate();

  const login = async (e) => {
    e.preventDefault();
    if (email === "" || password === "") {
      setAlert("Masukkan email & password anda!!");
    } else {
      try {
        const res = await signInWithEmailAndPassword(auth, email, password);
        const user = res.user;
        const docRef = doc(db, "users", user.uid);
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
          localStorage.setItem("token", user.accessToken);
          localStorage.setItem("uid", docSnap.id);
          localStorage.setItem("username", docSnap.data().username);
          localStorage.setItem("pp", docSnap.data().profile_picture);
          localStorage.setItem("authBy", docSnap.data().authProvider);
          localStorage.setItem("email", docSnap.data().email);
          alert("Login Berhasil");
          navigate("/home");
        } else {
          alert("Data tidak ditemukan");
        }
      } catch (error) {
        alert(error.message);
      }
    }
  };
  return (
    <div className="text-center my-4">
      <Button onClick={login} color="primary">
        Login
      </Button>
    </div>
  );
}

export default ButtonLogin;
