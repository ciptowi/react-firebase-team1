import React from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "reactstrap";
import { auth, db } from "../../config/firebase";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { addDoc, collection, query, where, getDocs } from "firebase/firestore";

function LoginGoogle(props) {
  const navigate = useNavigate();
  const googleProvider = new GoogleAuthProvider();
  const loginGoogle = async () => {
    try {
      const res = await signInWithPopup(auth, googleProvider);
      const user = res.user;
      const q = query(collection(db, "users"), where("uid", "==", user.uid));
      const docs = await getDocs(q);
      if (docs.docs.length === 0) {
        await addDoc(collection(db, "users"), {
          uid: user.uid,
          username: user.displayName,
          email: user.email,
          password: "",
          profile_picture: user.photoURL,
          level: "Easy",
          authProvider: "google",
        });
      }
      localStorage.setItem("token", user.accessToken);
      alert("Anda masuk dengan akun Google");
      navigate("/home");
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };
  return (
    <div className="text-center">
      <Button className="px-4" outline color="primary" onClick={loginGoogle}>
        <img
          src="./images/google.png"
          width="30px"
          className="mx-3"
          alt="google"
        />
        Continue with Google
      </Button>
    </div>
  );
}

export default LoginGoogle;
