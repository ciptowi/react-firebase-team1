import React from "react";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";
import "./css/Gameslider.css";

const Gameslider = () => {
  return (
    <div className="Gameslider">
      <h1>Choose your Game</h1>
      <div>
        <p>
          <span className="bold">All</span>
        </p>
        <p>Action</p>
        <p>RGP</p>
        <p>Thriller</p>
      </div>
      <div className="container">
        <img
          src="https://cdn.pixabay.com/photo/2019/09/23/16/06/war-4499000_960_720.jpg"
          className="pilihan"
          alt="action game"
        />
        <img
          src="https://i.pinimg.com/564x/28/f9/3a/28f93a766288438a993239755b1cf2c9.jpg"
          className="pilihan"
          alt="RPG"
        />
        <img
          src="https://i.pinimg.com/564x/09/8e/6b/098e6bbe79d3bac42fdda10d7f18c275.jpg"
          className="pilihan"
          alt="Thriller"
        />
      </div>
      <Link to={"/game-list"}>
        <Button color="warning" size="sm">
          View All
        </Button>
      </Link>
    </div>
  );
};

export default Gameslider;
