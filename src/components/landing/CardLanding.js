import { useState, useEffect } from "react";
import { query, collection, getDocs } from "firebase/firestore";
import { db } from "../../config/firebase";
import { Row, Col, Button, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./css/CardLanding.css";

const CardLanding = () => {
  const [orders, setOrders] = useState([]);
  const [user, setUser] = useState(false);

  const checkUser = () => {
    const token = localStorage.getItem("token");
    token ? setUser(true) : setUser(false);
  };

  useEffect(() => {
    async function fetchData() {
      try {
        let array = [];
        const q = query(collection(db, "games"));
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
          array.push({ ...doc.data(), id: doc.id });
        });
        setOrders(array);
      } catch (e) {
        //console.log(e);
      }
    }
    checkUser();
    fetchData();
  }, []);

  function ListGame() {
    return (
      <>
        {orders.map((order) => (
          <li key={order.id}>
            {user ? (
              <Link to={"/detailgame/" + order.id}>
                <Button variant="light" className="my-2 p-0">
                  <Image thumbnail src={order.thumbnail} alt={order.name} className="landing" />
                </Button>
              </Link>
            ) : (
              <Link to={"/login"}>
                <Button variant="light" className="my-2 p-0">
                  <Image thumbnail src={order.thumbnail} alt={order.name} className="landing" />
                </Button>
              </Link>
            )}
          </li>
        ))}
      </>
    );
  }

  return (
    <>
      <div>
        <Row className="text-center mx-auto">
          <Col>
            <h4 className="judul">Top Game</h4>
            <ListGame />
          </Col>
          <Col>
            <h4 className="judul">New Release</h4>
            <ListGame />
          </Col>
          <Col>
            <h4 className="judul">Coming Soon</h4>
            <ListGame />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default CardLanding;
