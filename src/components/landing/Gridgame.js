import { useState, useEffect } from "react";
import { Card, CardGroup, Button } from "react-bootstrap";
import { query, collection, getDocs } from "firebase/firestore";
import { Link } from "react-router-dom";
import { db } from "../../config/firebase";
import "./css/Gridgame.css";

function Gridgame(props) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        let array = [];
        const q = query(collection(db, "updates"));
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
          array.push({ ...doc.data(), id: doc.id });
        });
        setOrders(array);
      } catch (e) {
        //console.log(e);
      }
    }
    fetchData();
  }, []);

  return (
    <>
      <h2 className="judul1">New Updated Games</h2>
      <CardGroup>
        {orders.map((order) => (
          <Card key={order.id} className="mx-3">
            <Card.Img variant="top" src={order.thumbnail} />
            <Card.Body>
              <Card.Title>{order.game}</Card.Title>
              <div>Version: {order.version}</div>
              <div>{order.release_date}</div>
            </Card.Body>
            <Link to={"/updates/" + order.id} className="text-center">
              <Button color="warning" size="sm">
                View
              </Button>
            </Link>
          </Card>
        ))}
      </CardGroup>
    </>
  );
}

export default Gridgame;
