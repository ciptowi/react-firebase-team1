import { useState, useEffect } from "react";
import { query, collection, getDocs } from "firebase/firestore";
import { db } from "../../config/firebase";
import { Link } from "react-router-dom";
import { Carousel } from "react-bootstrap";
import "./css/Heroslider.css";

function Heroslider(props) {
  const [orders, setOrders] = useState([]);
  const [user, setUser] = useState(false);

  const checkUser = () => {
    const token = localStorage.getItem("token");
    token ? setUser(true) : setUser(false);
  };

  useEffect(() => {
    async function fetchData() {
      try {
        let array = [];
        const q = query(collection(db, "games"));
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
          array.push({ ...doc.data(), id: doc.id });
        });
        setOrders(array);
      } catch (e) {
        //console.log(e);
      }
    }
    checkUser();
    fetchData();
  }, []);

  return (
    <>
      <Carousel>
        {orders.map((order) => (
          <Carousel.Item interval={1000} key={order.id}>
            {user ? (
              <Link to={"/detailgame/" + order.id}>
                <img className="d-block w-100 heropict" src={order.thumbnail} alt={order.developer} />
              </Link>
            ) : (
              <Link to={"/login"}>
                <img className="d-block w-100 heropict" src={order.thumbnail} alt={order.developer} />
              </Link>
            )}
            <Carousel.Caption>
              <h3>{order.name}</h3>
              <h6>{order.genre}</h6>
              <p>Author: {order.developer}</p>
            </Carousel.Caption>
          </Carousel.Item>
        ))}
      </Carousel>
    </>
  );
}

export default Heroslider;
