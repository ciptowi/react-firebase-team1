import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Container,
  CardImg,
  Button,
  Row,
  Col,
} from "reactstrap";

function Cardlist() {
  return (
    <div>
      <h3>Detail Game</h3>
        <Container className="">
          <Row>
            <Col >
              <Card style={{ width: "18rem", marginTop:"20px", marginRight:"2px" }}>
                <CardImg variant="top" src="holder.js/100px180" />
                <CardBody>
                  <CardTitle>Card Title</CardTitle>
                  <CardText>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </CardText>
                  <Button variant="primary">Go somewhere</Button>
                </CardBody>
              </Card>
            </Col>
            <Col >
              <Card style={{ width: "18rem", marginTop:"20px" }}>
                <CardImg variant="top" src="holder.js/100px180" />
                <CardBody>
                  <CardTitle>Card Title</CardTitle>
                  <CardText>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </CardText>
                  <Button variant="primary">Go somewhere</Button>
                </CardBody>
              </Card>
            </Col>
            <Col >
              <Card style={{ width: "18rem", marginTop:"20px" } }>
                <CardImg variant="top" src="holder.js/100px180" />
                <CardBody>
                  <CardTitle>Card Title</CardTitle>
                  <CardText>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </CardText>
                  <Button variant="primary">Go somewhere</Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col >
              <Card style={{ width: "18rem", marginTop:"20px" }}>
                <CardImg variant="top" src="holder.js/100px180" />
                <CardBody>
                  <CardTitle>Card Title</CardTitle>
                  <CardText>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </CardText>
                  <Button variant="primary">Go somewhere</Button>
                </CardBody>
              </Card>
            </Col>
            <Col>
              <Card style={{ width: "18rem", marginTop:"20px" }}>
                <CardImg variant="top" src="holder.js/100px180" />
                <CardBody>
                  <CardTitle>Card Title</CardTitle>
                  <CardText>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </CardText>
                  <Button variant="primary">Go somewhere</Button>
                </CardBody>
              </Card>
            </Col>
            <Col >
              <Card style={{ width: "18rem", marginTop:"20px" } }>
                <CardImg variant="top" src="holder.js/100px180" />
                <CardBody>
                  <CardTitle>Card Title</CardTitle>
                  <CardText>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </CardText>
                  <Button variant="primary">Go somewhere</Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
  );
}

export default Cardlist;
