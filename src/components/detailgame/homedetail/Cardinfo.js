import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, CardBody, CardTitle, CardText } from "reactstrap";

function CardInfo() {
  return (
    <div>
      <h3>Detail Game</h3>
      <div
        style={{
          display: "block",
          width: 400,
          height: 400,
        }}
      >
        <Card>
          <CardBody>
            <CardTitle tag="h5">Sample Card title</CardTitle>
            <div className="scrollspy-example" data-bs-spy="scroll">
              <CardText>
                Sample Card Text to display!Sample Card Text to displaySample Card Text to displaySample Card Text to
                displaySample Card Text to displaySample Card Text to displaySample Card Text to displaySample Card Text
                to displaySample Card Text to displaySample Card Text to display Sample Card Text to displaySample Card
                Text to displaySample Card Text to displaySample Card Text to displaySample Card Text to displaySample
                Card Text to displaySample Card Text to displaySample Card Text to displaySample Card Text to
                displaySample Card Text to displaySample Card Text to displaySample Card Text to displaySample Card Text
                to displaySample Card Text
              </CardText>
            </div>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}

export default CardInfo;
