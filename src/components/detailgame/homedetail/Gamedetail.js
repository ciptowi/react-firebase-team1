import React from "react";
import { Container, Row, Col } from "reactstrap";
import Carousel from "./Carousel";
import Card from "./Cardinfo";
import Cardlist from "./Cardlist";



const HomeBody = () => {
  return (
    <Container>
      <Row>
        <Col sm={8}>
          <Carousel></Carousel>
        </Col>
        <Col sm={4}>
          <Card></Card>
        </Col>
        <Col col-sm={12}>
          <Cardlist></Cardlist>
        </Col>
      </Row>
    </Container>
  );
};
export default HomeBody;
