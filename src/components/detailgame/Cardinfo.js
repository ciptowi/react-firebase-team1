import { useEffect, useState } from "react";
import { Card, CardBody, CardTitle, CardText, Button } from "reactstrap";
import { db } from "../../config/firebase";
import { doc, getDoc } from "firebase/firestore";
import { useParams, useNavigate } from "react-router-dom";
import swal from "sweetalert";

function CardInfo() {
  const [name, setName] = useState("");
  const [dev, setDev] = useState("");
  const [genre, setGenre] = useState("");
  const [date, setDate] = useState("");
  const param = useParams();
  const id = param.id;
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchMyAPI() {
      const docRef = doc(db, "games", id);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        setName(docSnap.data().name);
        setGenre(docSnap.data().genre);
        setDate(docSnap.data().release_date);
        setDev(docSnap.data().developer);
      } else {
      }
    }
    fetchMyAPI();
  });

  return (
    <div>
      <h3>Detail Game</h3>
      <div
        style={{
          display: "block",
          width: 400,
          height: 220,
        }}
      >
        <Card>
          <CardBody>
            <CardTitle tag="h5">
              <b>{name}</b>
            </CardTitle>
            <div className="scrollspy-example" data-bs-spy="scroll">
              <CardText>
                <div style={{ margin: "5px" }}>
                  <b>Developer : {dev}</b>
                </div>
                <div style={{ margin: "5px" }}>
                  <b>Genre : {genre}</b>
                </div>
                <div style={{ margin: "5px" }}>
                  <b>Release Date : {date}</b>
                </div>
                <div style={{ margin: "5px" }}>
                  <Button
                    color="primary"
                    onClick={() => {
                      if (id === "l9Ay2BQwtsJc7kfgfOp7") {
                        navigate("/rps");
                      } else {
                        swal("Error", "Mohon maaf saat ini game sedang maintenance", "error");
                      }
                    }}
                  >
                    Play Now
                  </Button>
                </div>
              </CardText>
            </div>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}

export default CardInfo;
