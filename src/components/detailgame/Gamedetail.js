import React from "react";
import { Container, Row, Col } from "reactstrap";
import ImgDetail from "./ImgDetail";
import Card from "./Cardinfo";
import CardUser from "./CardUser";

const HomeBody = () => {
  return (
    <Container>
      <Row>
        <Col sm={8} style={{ height: "220px" }}>
          <ImgDetail />
        </Col>
        <Col sm={4}>
          <CardUser />
          <Card></Card>
        </Col>
      </Row>
    </Container>
  );
};
export default HomeBody;
