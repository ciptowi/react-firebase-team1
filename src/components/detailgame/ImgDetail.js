import { useEffect, useState } from "react";
import { db } from "../../config/firebase";
import { doc, getDoc } from "firebase/firestore";
import { useParams } from "react-router-dom";

function ImgDetail() {
  const [data, setData] = useState("");
  const param = useParams();
  const id = param.id;

  async function fetchMyAPI() {
    const docRef = doc(db, "games", id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      setData(docSnap.data());
    } else {
      //console.log("No such document!");
    }
  }

  useEffect(() => {
    fetchMyAPI();
  });

  return (
    <div>
      <img className="img-fluid mt-5" src={data.thumbnail} alt={data.name} />
    </div>
  );
}

export default ImgDetail;
