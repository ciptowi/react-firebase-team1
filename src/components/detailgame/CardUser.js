import { useEffect, useState } from "react";
import { Card, CardBody, CardTitle, CardText } from "reactstrap";
import { db } from "../../config/firebase";
import { doc, getDoc } from "firebase/firestore";
import { auth } from "../../config/firebase";

function CardUser() {
  const [game, setGame] = useState("");
  const [userID, setUserID] = useState("");
  const [user, setUser] = useState("");

  useEffect(() => {
    async function getId() {
      auth.onAuthStateChanged((user) => {
        try {
          setUserID(user.uid);
          getUser();
        } catch (error) {
          //console.log(error.message);
        }
      });
      //return getId;
    }

    async function getUser() {
      try {
        const docRef = doc(db, "users", userID);
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
          setUser(docSnap.data());
          getGame();
        } else {
          //console.log("No such document!");
        }
      } catch (error) {
        //console.log(error.message);
      }
      //return getUser;
    }

    async function getGame() {
      try {
        const docRef = doc(db, "rps_game_points", userID);
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
          setGame(docSnap.data());
        } else {
          //console.log("No such document!");
        }
      } catch (error) {
        //console.log(error.message);
      }
      //return getGame;
    }

    getId();
  });

  return (
    <div>
      <h3>Score</h3>
      <div
        style={{
          display: "block",
          width: 400,
          height: 180,
        }}
      >
        <Card>
          <CardBody>
            <CardTitle tag="h5">
              <b>{user.username}</b>
            </CardTitle>
            <div className="scrollspy-example" data-bs-spy="scroll">
              <CardText>
                <div style={{ margin: "5px" }}>
                  <b>Game : {game.game} </b>
                </div>
                <div style={{ margin: "5px" }}>
                  <b>Last Play : {game.updated_at}</b>
                </div>
                <div style={{ margin: "5px" }}>
                  <b>Score : {game.total}</b>
                </div>
                <div style={{ margin: "5px" }}>{/* <b>Player : {game.username}</b> */}</div>
              </CardText>
            </div>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}

export default CardUser;
