import React, { Component } from "react";
import { Container, Row } from "reactstrap";
import { useParams } from "react-router-dom";
import { doc, getDoc } from "firebase/firestore";
import { db } from "../../config/firebase";

function withParams(Component) {
  return (props) => <Component {...props} params={useParams()} />;
}

class Body extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0, items: [] };
  }

  toDateTime(secs) {
    var t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
  }

  async fetch() {
    let { id } = this.props.params;
    const docRef = doc(db, "updates", id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      this.setState({
        items: docSnap.data(),
      });
    } else {
      // doc.data() will be undefined in this case
      //console.log("No such document!");
    }
  }

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (
      //items.map((item, idx) => {
      <Container>
        <Row>
          <div>
            <style>
              {`.leaderboards {
              max-width: 100%;
              display: flex;
              padding: 10px;
              flex-direction: column;
              justify-content: center;
              background: #EEEEEE;
            }
            a {
              text-decoration: none;
              color: inherit;
            }
            a:hover {
              color:inherit; 
              text-decoration:none; 
              cursor:pointer;
            }`}
            </style>
            <h3>{this.state.items.game}</h3>
            <div className="container d-flex leaderboards">
              <div className="row">
                <div className="col-md-8">
                  <img
                    alt="Thumbnail Game"
                    className="img-fluid"
                    style={{ height: 200 }}
                    src={this.state.items.thumbnail}
                  ></img>
                </div>
                <div className="col-md-4">
                  <h3>Detail</h3>
                  <p>Updated on : {this.state.items.release_date}</p>
                  <p>Version : {this.state.items.version}</p>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-md-12">{this.state.items.description}</div>
              </div>
            </div>
          </div>
        </Row>
      </Container>
      //});
    );
  }
}

export default withParams(Body);
