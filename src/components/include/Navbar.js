import React, { Component } from "react";
import { Collapse, Navbar, NavbarToggler, Nav, NavItem } from "reactstrap";
import { Link } from "react-router-dom";
import { auth } from "../../config/firebase";
import { signOut } from "firebase/auth";

class NavbarComponent extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isAuthenticated: false,
      isOpen: false,
      username: "",
      pp: "",
      email: "",
      uid: "",
    };
  }

  componentWillMount() {
    this.checkUser();
  }

  checkUser = () => {
    const token = localStorage.getItem("token");
    if (!!token) {
      return this.setState({ isAuthenticated: true });
    }
  };

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  logout() {
    signOut(auth);
    localStorage.removeItem("token");
  }

  componentDidMount() {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          loading: false,
          username: localStorage.getItem("username"),
          pp: localStorage.getItem("pp"),
          authBy: localStorage.getItem("authBy"),
          email: user.email,
          uid: user.uid,
          authenticated: true,
        });
      } else {
        this.setState({ loading: false, authenticated: false });
      }
    });
  }

  render() {
    return (
      <div>
        <Navbar color="light" expand="md" id="navbar-global" light>
          <NavbarToggler onClick={function noRefCheck() {}} />
          <Collapse navbar>
            <Nav className="me-auto" navbar>
              <NavItem>
                <Link
                  to={"/home"}
                  style={{
                    paddingLeft: "75px",
                    textDecoration: "none",
                    color: "black",
                  }}
                >
                  Home{" "}
                </Link>
              </NavItem>
              <NavItem>
                <Link to={`/profile/${this.state.uid}`} style={{ paddingLeft: "10px", color: "blue" }}>
                  Profile
                </Link>
              </NavItem>
              <NavItem>
                <Link
                  to={"/game-list"}
                  style={{
                    paddingLeft: "10px",
                    textDecoration: "none",
                    color: "black",
                  }}
                >
                  Game List
                </Link>
              </NavItem>
            </Nav>
            {this.state.authenticated ? (
              <Nav className="ms-auto" navbar>
                <Link to={`/profile/${this.state.uid}`} style={{ paddingRight: "15px" }}>
                  {this.state.pp == null ? (
                    <img
                      src={window.location.origin + "/images/user.png"}
                      width="30"
                      height="30"
                      className="d-inline-block align-top"
                      alt="avatar"
                    />
                  ) : (
                    <img alt="avatar" src={this.state.pp} width="30" height="30" className="d-inline-block align-top" />
                  )}
                </Link>
                <NavItem>
                  <Link to={`/profile/${this.state.uid}`} style={{ paddingRight: "15px", verticalAlign: "middle" }}>
                    {this.state.username !== null ? this.state.username : this.state.email}
                  </Link>
                </NavItem>
                <NavItem>
                  {this.state.username !== null || this.state.email !== null ? "" : ""}
                  <Link to="/" onClick={this.logout} style={{ paddingRight: "100px", verticalAlign: "middle" }}>
                    Logout
                  </Link>
                </NavItem>
              </Nav>
            ) : (
              <Nav className="ms-auto" navbar>
                <NavItem>
                  {this.state.username !== null || this.state.email !== null ? "" : ""}
                  <Link to="/login" style={{ paddingRight: "100px", verticalAlign: "middle" }}>
                    Login
                  </Link>
                </NavItem>
              </Nav>
            )}
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default NavbarComponent;
