import React from "react";
import { Box } from "./FooterStyles";
import {
  FaFacebook,
  FaInstagram,
  FaTwitter,
  FaPinterest,
} from "react-icons/fa";

const Footer = () => {
  return (
    <Box className="fixed-bottom">
      <div className="text-center pb-2">
        <FaFacebook className="icon mx-5" />
        <FaInstagram className="icon mx-5" />
        <span style={{ color: "black", textAlign: "center" }}>
          Binar FSW Kelompok 1
        </span>
        <FaTwitter className="icon mx-5" />
        <FaPinterest className="icon mx-5" />
      </div>
    </Box>
  );
};
export default Footer;
