import React from "react";
// import axios, { post } from 'axios';import { Component, useState } from "react";
// import { useNavigate, Link } from "react-router-dom";
import { updateDoc, doc, getDoc } from "firebase/firestore";
import { db, storage } from "../../config/firebase";
import { ref, getDownloadURL, uploadBytesResumable } from "firebase/storage";

import { Container, Row, Col, Form, FormGroup, Label, Input, Button, CardTitle, CardImg } from "reactstrap";
import { useParams } from "react-router-dom";

function withParams(Component) {
  return (props) => <Component {...props} params={useParams()} />;
}

class Body extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      filename: null,
      username: "",
      preview: "",
      curr_id: "",
      uid: "",
      level: "",
      disable: true,
      disableSave: false,
      style: true,
      prog: 0,
    };
    this.email = props.email;
    this.password = props.password;
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleImageAsFile = this.handleImageAsFile.bind(this);
    this.changeUsername = this.changeUsername.bind(this);
    this.updateData = this.updateData.bind(this);
    this.formHandler = this.formHandler.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  async onFormSubmit(e) {
    e.preventDefault(); // Stop form submit
    const new_username = this.state.username;
    const new_filename = this.state.filename;
    const uid = this.props.params;
    const data = doc(db, "users", uid.id);
    await updateDoc(data, {
      username: new_username,
      profile_picture: new_filename,
    });
  }

  uploadFiles(file) {
    //
    if (!file) return;
    const sotrageRef = ref(storage, `images/users/${file.name}`);
    const uploadTask = uploadBytesResumable(sotrageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const prog = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        //console.log(prog);
        this.setState({ prog: prog });
      },
      (error) => console.log(error),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
          this.setState({ filename: downloadURL });
          this.updateData();
        });
      }
    );
  }

  formHandler(e) {
    this.handleClick();
    e.preventDefault();
    const file = e.target[0].files[0];
    this.uploadFiles(file);
  }

  onChange(e) {
    const url = URL.createObjectURL(e.target.files[0]);
    this.setState({ file: e.target.files[0], preview: url });
  }

  handleImageAsFile(e) {
    const image = e.target.files[0];
    this.setState({ filename: image });
  }

  changeUsername(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  async updateData() {
    const uid = this.props.params;
    const new_username = this.state.username;
    let new_filename = this.state.filename;
    const preview = this.state.preview;
    if (new_filename === null) {
      new_filename = preview;
    }

    const data = doc(db, "users", uid.id);
    await updateDoc(data, {
      username: new_username,
      profile_picture: new_filename,
    });
    localStorage.setItem("username", new_username);
    localStorage.setItem("pp", new_filename);
    alert("Data Berhasil Diubah!");
    window.location.reload(false);
  }
  async fetch() {
    const uid = this.props.params;
    let user_id = "";
    let curr = localStorage.getItem("uid");

    if (curr !== uid.id) {
      const docRef = doc(db, "leaderboards", uid.id);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        user_id = docSnap.data().uid;
        const docRef2 = doc(db, "users", user_id);
        const docSnap2 = await getDoc(docRef2);
        if (docSnap2.exists()) {
          this.setState({
            username: docSnap2.data().username,
            preview: docSnap2.data().profile_picture,
            level: docSnap2.data().level,
          });
        } else {
          // doc.data() will be undefined in this case
          // console.log("No such document!");
        }
      }
    } else {
      const docRef = doc(db, "users", uid.id);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        this.setState({
          username: docSnap.data().username,
          preview: docSnap.data().profile_picture,
          level: docSnap.data().level,
        });
      } else {
        // doc.data() will be undefined in this case
        //console.log("No such document!");
      }
    }
    this.setState({
      curr_id: localStorage.getItem("uid"),
      uid: uid.id,
    });
  }

  componentDidMount() {
    this.fetch();
  }

  handleClick() {
    this.setState({ disable: !this.state.disable, disableSave: !this.state.disableSave, style: !this.state.style });
  }

  // class Body extends Component {
  render() {
    const { username, preview, curr_id, uid, level, disable, disableSave } = this.state;
    const style = this.state.style ? { display: "none" } : { display: "block" };
    const text = disableSave ? "Cancel" : "Edit";
    return (
      <Container>
        <Row>
          <Col className="col-sm-6 col-lg-4 col-xl-4 mx-auto align-item-center">
            <CardTitle tag="h2" style={{ textAlign: "center" }}>
              Gamer Profile
            </CardTitle>
            {preview === null ? (
              <CardImg
                alt="Card image cape"
                src={window.location.origin + "/images/user.png"}
                top
                width="50%"
                style={{
                  width: "200px",
                  height: "200px",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              ></CardImg>
            ) : (
              <CardImg
                alt="Card image cap"
                src={preview}
                top
                width="50%"
                style={{
                  width: "200px",
                  height: "200px",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                onClick={this.chooseImage}
              ></CardImg>
            )}
            {curr_id === uid ? (
              <div>
                {/* <form onSubmit={this.formHandler}>
                  <input type="file" onChange={this.onChange} />
                  <br></br>
                  <button type="submit">Ganti Photo</button>
                </form> */}

                <Form className="form" onSubmit={this.formHandler}>
                  <FormGroup>
                    <Input type="file" id="file" name="image" style={style} onChange={this.onChange}></Input>
                    <Label for="exampleEmail">Username</Label>
                    <Input
                      name="username"
                      placeholder="RinaldiGamer77"
                      value={username}
                      onChange={this.changeUsername}
                      disabled={disable}
                    />
                  </FormGroup>

                  <div style={{ float: "right" }}>
                    <Button
                      style={{ color: "#fff", backgroundColor: "#0d6efd", borderColor: "#0d6efd" }}
                      onClick={this.handleClick}
                    >
                      {text}
                    </Button>
                    <Button
                      style={{
                        color: "#fff",
                        backgroundColor: "#0d6efd",
                        borderColor: "#0d6efd",
                        marginLeft: "10px",
                      }}
                      type="submit"
                      disabled={disable}
                    >
                      Simpan Perubahan
                    </Button>
                  </div>
                </Form>
              </div>
            ) : (
              <div>
                <Label for="exampleEmail">Username</Label>
                <Input name="username" placeholder="RinaldiGamer77" value={username} disabled={disable} />
                <Label for="exampleEmail">Level</Label>
                <Input name="level" placeholder="RinaldiGamer77" value={level} disabled={disable} />
              </div>
            )}
            {/* <form onSubmit={this.formHandler}>
              <input type="file" onChange={this.onChange} />
              <br></br>
              <button type="submit">Ganti Photo</button>
            </form>
            <Form className="form">
              <FormGroup>
                <Label for="exampleEmail">Username</Label>
                <Input name="username" placeholder="RinaldiGamer77" value={username} onChange={this.changeUsername} />
              </FormGroup> */}
            {/* <FormGroup>
                <Label for="examplePassword">Password</Label>
                <Input type="password" name="password" id="examplePassword" placeholder="********" />
              </FormGroup> */}
            {/* <Button onClick={this.updateData}>Simpan Perubahan</Button>
            </Form> */}
          </Col>
        </Row>
        <br></br>
        {/*<CardTitle tag="h2">Game History</CardTitle>
         <CardGroup>
          <Card>
            <CardImg alt="Card image cap" src="https://picsum.photos/318/180" top width="100%" />
            <CardBody>
              <CardTitle tag="h5">Game 1</CardTitle>
              <CardSubtitle className="mb-2 text-muted" tag="h6">
                Level Permainan
              </CardSubtitle>
              <CardText>
                This is a wider card with supporting text below as a natural lead-in to additional content. This content
                is a little bit longer.
              </CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg alt="Card image cap" src="https://picsum.photos/318/180" top width="100%" />
            <CardBody>
              <CardTitle tag="h5">Game 2</CardTitle>
              <CardSubtitle className="mb-2 text-muted" tag="h6">
                Level permainan
              </CardSubtitle>
              <CardText>This card has supporting text below as a natural lead-in to additional content.</CardText>
            </CardBody>
          </Card>
          <Card>
            <CardImg alt="Card image cap" src="https://picsum.photos/318/180" top width="100%" />
            <CardBody>
              <CardTitle tag="h5">Game 3</CardTitle>
              <CardSubtitle className="mb-2 text-muted" tag="h6">
                Level Permainan
              </CardSubtitle>
              <CardText>
                This is a wider card with supporting text below as a natural lead-in to additional content. This card
                has even longer content than the first to show that equal height action.
              </CardText>
            </CardBody>
          </Card>
        </CardGroup> */}
      </Container>
    );
  }
}

export default withParams(Body);
