import React, { Component } from "react";
import NavbarComponent from "../components/include/Navbar";
import Footer from "../components/include/Footer";
import BodyDetail from "../components/updates/Body";

class Home extends Component {
  render() {
    return (
      <div>
        <NavbarComponent></NavbarComponent>
        <BodyDetail></BodyDetail>
        <Footer></Footer>
      </div>
    );
  }
}

export default Home;
