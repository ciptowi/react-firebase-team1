import { useState } from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Card,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
} from "reactstrap";
import LoginGoogle from "../components/auth/LoginGoogle";
import ButtonRegister from "../components/auth/ButtonRegister";
import LoginFacebook from "../components/auth/LoginFacebook";

function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [alert, setAlert] = useState("");

  const changeUsername = (e) => {
    const value = e.target.value;
    setUsername(value);
    setAlert("");
  };

  const changeEmail = (e) => {
    const value = e.target.value;
    setEmail(value);
    setAlert("");
  };

  const changePassword = (e) => {
    const value = e.target.value;
    setPassword(value);
    setAlert("");
  };

  return (
    <Container>
      <Row className="mt-5 ">
        <Col className="col-sm-6 col-lg-4 col-xl-4 mx-auto align-item-center">
          <Card body outline color="secondary">
            <h5 className="text-center">Register</h5>
            <div className="d-grid gap-2 mx-auto pt-4 mb-3">
              <LoginGoogle />
              <LoginFacebook />
            </div>
            <div className="text-center mb-2">or</div>
            {alert ? <Alert color="danger">{alert}</Alert> : ""}
            <Form>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="exampleEmail" className="mr-sm-2">
                  Username :
                </Label>
                <Input
                  type="text"
                  name="username"
                  placeholder="username"
                  value={username}
                  onChange={changeUsername}
                />
              </FormGroup>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="exampleEmail" className="mr-sm-2">
                  Email :
                </Label>
                <Input
                  type="email"
                  name="email"
                  placeholder="sample@email.com"
                  value={email}
                  onChange={changeEmail}
                />
              </FormGroup>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="examplePassword" className="mr-sm-2">
                  Password :
                </Label>
                <Input
                  type="password"
                  name="password"
                  placeholder="Password"
                  value={password}
                  onChange={changePassword}
                />
              </FormGroup>
              <ButtonRegister
                username={username}
                email={email}
                password={password}
                setAlert={setAlert}
              />
            </Form>
            <div>
              Have accout? <Link to={"/login"}>Login</Link>
            </div>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Register;
