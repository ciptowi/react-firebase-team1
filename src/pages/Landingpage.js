import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import CardLanding from "../components/landing/CardLanding";
import Gridgame from "../components/landing/Gridgame";
import Heroslider from "../components/landing/Heroslider";
import Gameslider from "../components/landing/Gameslider";
import "../components/landing/css/Landingpage.css";
import NavbarComponent from "../components/include/Navbar";
import Footer from "../components/include/Footer";

class Landingpage extends Component {
  render() {
    return (
      <div>
        <NavbarComponent />
        <Heroslider></Heroslider>
        <Container fluid>
          <Row>
            <Col auto className="background">
              <CardLanding></CardLanding>
            </Col>
          </Row>
          <Row>
            <Col auto className="second">
              <Gridgame></Gridgame>
            </Col>
          </Row>
          <Row>
            <Col auto className="second">
              <Gameslider></Gameslider>
            </Col>
          </Row>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default Landingpage;
