import { useState } from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Card,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
} from "reactstrap";
import ButtonLogin from "../components/auth/ButtonLogin";
import LoginFacebook from "../components/auth/LoginFacebook";
import LoginGoogle from "../components/auth/LoginGoogle";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [alert, setAlert] = useState("");

  const changeEmail = (e) => {
    const value = e.target.value;
    setEmail(value);
    setAlert("");
  };

  const changePassword = (e) => {
    const value = e.target.value;
    setPassword(value);
    setAlert("");
  };

  return (
    <div>
      <Container>
        <Row className="mt-5 ">
          <Col className="col-sm-6 col-lg-4 col-xl-4 mx-auto align-item-center">
            <Card body outline color="secondary">
              <h4 className="text-center">Login</h4>
              <div className="d-grid gap-2 mx-auto pt-4 mb-3">
                <LoginGoogle />
                <LoginFacebook />
              </div>
              <div className="text-center mb-2">or</div>
              {alert ? <Alert color="danger">{alert}</Alert> : ""}
              <Form>
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                  <Label for="exampleEmail" className="mr-sm-2">
                    Email :
                  </Label>
                  <Input
                    type="email"
                    name="email"
                    placeholder="example@email.com"
                    value={email}
                    onChange={changeEmail}
                  />
                </FormGroup>
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                  <Label for="examplePassword" className="mr-sm-2">
                    Password :
                  </Label>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={changePassword}
                  />
                </FormGroup>
                <ButtonLogin email={email} password={password} />
              </Form>
              <div>
                No accout yet?{" "}
                <Link to={"/register"}>Register a new account</Link>
              </div>
              <div>
                Forgot your password?{" "}
                <Link to={"/forgote"}>Reset your password</Link>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Login;
