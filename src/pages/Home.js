import React, { Component } from "react";
import NavbarComponent from "../components/include/Navbar";
import Footer from "../components/include/Footer";
import HomeBody from "../components/home/Body";

class Home extends Component {
  state = { isAuthenticated: false };
  componentWillMount() {
    this.checkUser();
  }

  checkUser = () => {
    const token = localStorage.getItem("token");
    if (!!token) {
      return this.setState({ isAuthenticated: true });
    }
  };

  render() {
    const { isAuthenticated } = this.state;
    if (!isAuthenticated) {
      window.location = "/login";
    }
    return (
      <div>
        <NavbarComponent></NavbarComponent>
        <HomeBody></HomeBody>
        <Footer></Footer>
      </div>
    );
  }
}

export default Home;
