import React, { Component } from "react";
import Gamedetail from "../components/detailgame/Gamedetail";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "../components/include/Navbar";
import Footer from "../components/include/Footer";

class game extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Gamedetail></Gamedetail>
        <Footer />
      </div>
    );
  }
}

export default game;
