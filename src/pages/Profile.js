import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Body from "../components/profile/Body";
import NavbarComponent from "../components/include/Navbar";
import Footer from "../components/include/Footer";

class Profile extends Component {
  state = { isAuthenticated: false };
  componentWillMount() {
    this.checkUser();
  }

  checkUser = () => {
    const token = localStorage.getItem("token");
    if (!!token) {
      return this.setState({ isAuthenticated: true });
    }
  };
  render() {
    const { isAuthenticated } = this.state;
    if (!isAuthenticated) {
      window.location = "/login";
    }
    return (
      <div>
        <NavbarComponent></NavbarComponent>
        <Body></Body>
        <Footer></Footer>
      </div>
    );
  }
}

export default Profile;
