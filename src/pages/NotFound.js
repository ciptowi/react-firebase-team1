import React from "react";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";

function NotFound() {
  const nevigate = useNavigate();

  function goToLandingpage() {
    nevigate("/");
  }
  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div className="d-inline text-center">
        <strong className="display-1 fw-bold">404</strong>
        <h1>Page Not Found !</h1>
        <Button
          onClick={goToLandingpage}
          outline
          color="primary"
          className="mt-3"
        >
          Kembali ke Beranda
        </Button>
      </div>
    </div>
  );
}

export default NotFound;
