import React, { Component } from "react";
import Footer from "../components/include/Footer";
import NavbarComponent from "../components/include/Navbar";
import Content from "../components/list/Content";
import DetailList from "../components/list/DetailList";

class List extends Component {
  render() {
    return (
      <div>
        <NavbarComponent />
        <Content />
        <DetailList />
        <Footer />
      </div>
    );
  }
}

export default List;
